import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
// import React from 'react';
import { toast } from 'react-toastify';
import Dropzone from 'react-dropzone';
import ReactDOM from 'react-dom';
import {
    Accordion,
    AccordionItem,
    AccordionItemTitle,
    AccordionItemBody,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import { RadioGroup, Radio } from 'react-radio-group';
// import DropdownButton from 'react-bootstrap/DropdownButton'
import ReactTable from "react-table";
import 'react-table/react-table.css'

class App extends Component {
  constructor(props) {
    super(props);
    this.selectedRole = {}
    this.state = {
        isEdit: false,
        selectedRecord: {},
        showDeleteRole: false,
        dataSource: [],
        showModal: false,
        limit: 5,
        page: 1
    }
}
  render() {
    return (
      <React.Fragment>
      <Accordion>
          <AccordionItem>
              <AccordionItemTitle>
                  <h3>Available Events</h3>
              </AccordionItemTitle>
              <AccordionItemBody>
                  {/* <p>Body content</p> */}
                  <Accordion>
                      <AccordionItem>
                          <AccordionItemTitle>
                              <h3>Search Events</h3>
                          </AccordionItemTitle>
                          <span >
                              <AccordionItemBody>
                                  Body content1
                              <RadioGroup style={{ display: "-webkit-inline-box", paddingLeft: "50px" }} name="fruit" selectedValue={this.state.selectedValue} onChange={this.handleChange}>
                                  <Radio value="apple" />Apple
                                  <Radio value="orange" />Orange
                                  <Radio value="watermelon" />Watermelon
                              </RadioGroup>
                              <div class ="marginTop">
                            <button class="buttonWidth">sample</button>
                            </div>
                              </AccordionItemBody>
                          </span>
                      </AccordionItem>
                      <AccordionItem>
                          <AccordionItemTitle>
                              <h3>One time Events</h3>
                          </AccordionItemTitle>
                          <AccordionItemBody>
                              {/* <ReactTable data={data} columns={columns}/> */}
                          </AccordionItemBody>
                      </AccordionItem>
                      <AccordionItem>
                          <AccordionItemTitle>
                              <h3>Customer's Credit/Ordering Status</h3>
                          </AccordionItemTitle>
                          <AccordionItemBody>
                              <Accordion>
                                  <AccordionItem>
                                      <AccordionItemTitle>
                                          <h3>Credit Status</h3>
                                      </AccordionItemTitle>
                                      <AccordionItemBody style ={{paddingBottom:"100px"}}>
                                          <span class="col-md-12 col-lg-12" style={{ display: "inline-flex" }}> <p style={{ paddingRight: "20px" }}>Credit Limit : $100</p><p style={{ paddingRight: "20px" }}>Credit Limit : $100</p><p style={{ paddingRight: "20px" }}>Credit Limit : $100</p>
                                              <input type="checkbox" defaultChecked={this.state.chkbox} onChange={this.handleChangeChk} style={{ marginTop: "23px" }} />
                                              <p style={{ paddingRight: "20px" }}>Override Credit Limit</p>
                                          </span>
                                      </AccordionItemBody>
                                  </AccordionItem>
                                  <AccordionItem>
                                      <AccordionItemTitle>
                                          <h3>Restore PPV/On Demand</h3>
                                      </AccordionItemTitle>
                                      <AccordionItemBody>
                                          <div style={{ display: "inline-flex", marginRight: "20" }}> <button className="button button-secondary" type="button" > Restore PPV</button>
                                              <p style={{ marginLeft: "20" }}>Body content</p>
                                          </div>
                                      </AccordionItemBody>
                                      <AccordionItemBody>
                                          <Accordion>
                                              <AccordionItem>
                                                  <AccordionItemTitle>
                                                      <h3>Scripts</h3>
                                                  </AccordionItemTitle>
                                                  <AccordionItemBody>
                                                      <p>Read to customer</p>
                                                  </AccordionItemBody>
                                              </AccordionItem>
                                          </Accordion>
                                      </AccordionItemBody>
                                  </AccordionItem>
                              </Accordion>
                          </AccordionItemBody>
                      </AccordionItem>
                  </Accordion>
              </AccordionItemBody>
          </AccordionItem>
          <AccordionItem>
              <AccordionItemTitle>
                  <h3>Ordered Events</h3>
              </AccordionItemTitle>
              <AccordionItemBody>
                  <Accordion>
                      <AccordionItem>
                          <AccordionItemTitle>
                              <h3>One-Time Events</h3>
                          </AccordionItemTitle>
                          <AccordionItemBody style ={{paddingBottom:"150px"}}>
                          <div >
                              <div className="col-xs-6 col-sm-12 col-lg-6 col-xl-6 forPaddingLeft ">
                                   <label className="labels labelsFontSize commonProperties"> First Name <span className="mandatory-field">:</span></label>
                               </div>
                               <div className="col-xs-12 col-sm-12  col-lg-6 col-xl-6 withPaddingLeft">
                                   <label className="labels labelsFontSize commonProperties"> Last Name  <span className="mandatory-field">:</span></label>
                              </div>
                              <div className="col-xs-6 col-sm-12 col-lg-6 col-xl-6 forPaddingLeft ">
                                   <label className="labels labelsFontSize commonProperties"> First Name <span className="mandatory-field">:</span></label>
                               </div>
                               <div className="col-xs-12 col-sm-12  col-lg-6 col-xl-6 withPaddingLeft">
                                   <label className="labels labelsFontSize commonProperties"> Last Name  <span className="mandatory-field">:</span></label>
                              </div>
                              <div className="col-xs-6 col-sm-12 col-lg-6 col-xl-6 forPaddingLeft ">
                                   <label className="labels labelsFontSize commonProperties"> First Name <span className="mandatory-field">:</span></label>
                               </div>
                               <div className="col-xs-12 col-sm-12  col-lg-6 col-xl-6 withPaddingLeft">
                                   <label className="labels labelsFontSize commonProperties"> Last Name  <span className="mandatory-field">:</span></label>
                              </div>
                          </div>
                          </AccordionItemBody>
                      </AccordionItem>
                  </Accordion>
              </AccordionItemBody>
          </AccordionItem>
      </Accordion>
  </React.Fragment>
      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <p>
      //       Edit <code>src/App.js</code> and save to reload.
      //     </p>
      //     <a
      //       className="App-link"
      //       href="https://reactjs.org"
      //       target="_blank"
      //       rel="noopener noreferrer"
      //     >
      //       Learn React
      //     </a>
      //   </header>
      // </div>
    );
  }
}

export default App;
